package com.bookcake.imageslist.utils

import android.content.Context
import android.util.TypedValue
import android.view.WindowManager
import com.bookcake.imageslist.common.DIALOG_WIDTH
import kotlin.math.max

object ViewUtils {

    private fun ViewUtils() {
        throw IllegalStateException("Utility class")
    }

    fun dp2px(context: Context): Int {
        val minWidth = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            DIALOG_WIDTH.toFloat(),
            context.resources.displayMetrics
        ).toInt()
        val screenWidth =
            (ScreenUtils.getScreenSize(context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).width * 0.9).toInt()
        return max(screenWidth, minWidth)
    }
}