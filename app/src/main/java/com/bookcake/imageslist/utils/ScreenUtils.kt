package com.bookcake.imageslist.utils

import android.graphics.Point
import android.view.WindowManager
import java.lang.IllegalStateException

class ScreenUtils private constructor() {
    class ScreenSize internal constructor(var width: Int, var height: Int)
    companion object {
        fun getScreenSize(windowManager: WindowManager): ScreenSize {
            val display = windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            return ScreenSize(size.x, size.y)
        }
    }

    init {
        throw IllegalStateException("Utility class")
    }
}