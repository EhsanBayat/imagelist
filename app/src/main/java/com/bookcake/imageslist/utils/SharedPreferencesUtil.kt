package com.bookcake.imageslist.utils

import android.content.SharedPreferences
import com.bookcake.imageslist.common.NIGHT_THEME
import org.koin.core.KoinComponent
import org.koin.core.inject

object SharedPreferencesUtil : KoinComponent {
    private val sp: SharedPreferences by inject()

    var nightTheme: Boolean
        get() = get(NIGHT_THEME)
        set(value) = set(NIGHT_THEME, value)

    private fun get(key: String): Boolean {
        return sp.getBoolean(key, false)
    }

    private fun set(key: String, value: Boolean) {
        sp.edit().putBoolean(key, value).apply()
    }

}
