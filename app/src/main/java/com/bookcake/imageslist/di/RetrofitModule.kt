package com.bookcake.imageslist.di

import com.bookcake.imageslist.data.network.NetworkManagerInterface
import com.bookcake.imageslist.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val retrofitModule = module {

    single {
        /**
         * Provides and configs logger to see the logs in terminal.
         */
        HttpLoggingInterceptor().also {
            it.level = HttpLoggingInterceptor.Level.BODY
        }
    }

    single {
        return@single OkHttpClient().newBuilder().also {
            it.connectTimeout(5, TimeUnit.MINUTES)
            it.readTimeout(30, TimeUnit.SECONDS)
            it.callTimeout(30, TimeUnit.SECONDS)
            it.addInterceptor(get<HttpLoggingInterceptor>())

        }
    }

    single {
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_SERVER_URL)
            .addConverterFactory(get<GsonConverterFactory>())
            .client(get<OkHttpClient.Builder>().build())
            .build()
    }

    single {
        get<Retrofit>().create(NetworkManagerInterface::class.java)
    }
}
