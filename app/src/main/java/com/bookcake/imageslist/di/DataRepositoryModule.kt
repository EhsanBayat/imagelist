package com.bookcake.imageslist.di

import com.bookcake.imageslist.data.repository.ImagesListRepository
import org.koin.dsl.module

val dataRepositoryModule = module {

    single {
        ImagesListRepository(get())
    }
}
