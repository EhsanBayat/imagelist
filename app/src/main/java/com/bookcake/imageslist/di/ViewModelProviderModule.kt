package com.bookcake.imageslist.di

import com.bookcake.imageslist.ui.modules.images_list.ImagesListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { ImagesListViewModel() }
}
