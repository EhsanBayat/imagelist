package com.bookcake.imageslist.di

import com.google.gson.GsonBuilder
import org.koin.dsl.module
import retrofit2.converter.gson.GsonConverterFactory

val gsonConverterFactoryModule = module {
    single {
        GsonConverterFactory.create(get())
    }
}

val gsonModule = module {
    single {
        GsonBuilder().create()
    }
}
