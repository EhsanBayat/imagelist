package com.bookcake.imageslist.ui.modules.images_list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bookcake.imageslist.R
import com.bookcake.imageslist.extensions.imeSearchClick
import com.bookcake.imageslist.extensions.isConnectToInternet
import com.bookcake.imageslist.data.pojo.ImageBean
import com.bookcake.imageslist.data.pojo.SearchBean
import com.bookcake.imageslist.databinding.FragmentImagesListBinding
import com.bookcake.imageslist.ui.base.BaseFragment
import com.bookcake.imageslist.data.network.ResultWrapper

class ImagesListFragment : BaseFragment<FragmentImagesListBinding>(R.layout.fragment_images_list) {

    private val _viewModel: ImagesListViewModel by viewModels()
    private lateinit var imagesListAdapter: ImagesListAdapter
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showStatusBar()
        initToolbar()
        initViews()
    }

    private fun initToolbar() {
        showHideToolbarItems(hasTheme = true , toolbarTitle = R.string.app_name)
    }

    private fun initViews() {
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = _viewModel

            edtSearch.getEditText().imeSearchClick {
                fetchImages(edtSearch.text.toString())
            }

            imagesListAdapter = ImagesListAdapter { imageBean ->
                navigateToImageDetails(imageBean)
            }
            rvImages.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext())
                adapter = imagesListAdapter
            }
            fetchImages()
        }

    }

    private fun fetchImages(searchQuery : String = "yellow flower") {
        if(isConnectToInternet()) {
            _viewModel.fetchImages(searchQuery)
            _viewModel.searchResult.observe(viewLifecycleOwner, {
                when (it) {
                    is ResultWrapper.Success -> {
                        it.data?.let { searchBean ->
                            initImagesAdapter(searchBean)
                        }
                    }
                    is ResultWrapper.Failure -> {
                        handleException(it.message)
                    }
                }
            })
        }else {
            handleException("no internet connection!!!")
        }
    }

    private fun initImagesAdapter(searchResult : SearchBean) {
        if(searchResult.imagesList?.isNotEmpty() == true) {
            imagesListAdapter.submitList(searchResult.imagesList)
        }else {
            binding.rvImages.setEmptyView(binding.txtEmpty)
        }
    }

    private fun navigateToImageDetails(imageBean : ImageBean) {
        val action =
            ImagesListFragmentDirections.actionImagesListFragmentToImageDetailsFragment(imageBean)
        findNavController().navigate(action)
    }
}
