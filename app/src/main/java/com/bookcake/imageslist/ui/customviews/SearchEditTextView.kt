package com.bookcake.imageslist.ui.customviews

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import androidx.databinding.DataBindingUtil
import com.bookcake.imageslist.R
import com.bookcake.imageslist.extensions.getAutofillManager
import com.bookcake.imageslist.databinding.ViewSearchEdittextBinding

class SearchEditTextView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    init {
        context.obtainStyledAttributes(attrs, R.styleable.SearchEditTextView).apply {
            try {
                val hint: CharSequence? = getString(R.styleable.SearchEditTextView_hint)
                if (hint != null) setHint(hint.toString())
            } finally {
                recycle()
            }
        }
    }

    private var binding: ViewSearchEdittextBinding? = null

    override fun onFinishInflate() {
        super.onFinishInflate()
        initLayout()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getAutofillManager().disableAutofillServices()
            binding!!.edtInput.importantForAutofill = IMPORTANT_FOR_AUTOFILL_NO
        }

    }

    private fun initLayout() {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.view_search_edittext, this, true)
    }

    private fun setHint(textResId: String) {
        initLayout()
        binding!!.edtInput.hint = textResId
    }

    fun getEditText() = binding!!.edtInput

    var text: String?
        get() = binding!!.edtInput.text.toString()
        set(s) {
            binding!!.edtInput.setText(s)
        }

}