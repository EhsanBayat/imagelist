package com.bookcake.imageslist.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.bookcake.imageslist.ui.modules.main.MainActivity

abstract class BaseFragment<out VB: ViewDataBinding>(val layout: Int) : Fragment() {

    private var _binding: ViewDataBinding? = null

    @Suppress("UNCHECKED_CAST")
    protected val binding: VB
        get() = _binding as VB

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, s: Bundle?): View? {
        require(layout != -1) {
            "You have not provided the layout for the fragment," +
                    " Make sure to override the 'layout' property in your fragment"
        }
        _binding = DataBindingUtil.inflate(inflater, layout, parent, false)
        return requireNotNull(_binding).root
    }

    protected fun hideToolbar() {
        (activity as MainActivity).hideToolbar()
    }
    protected fun showHideToolbarItems(hasBack : Boolean? = null ,
                                       hasTheme : Boolean? = null,
                                        toolbarTitle : Int? = null) {
        (activity as MainActivity).showHideToolbarItems(hasBack,hasTheme,toolbarTitle)
    }

    protected fun handleException(message: String) {
        Toast.makeText(context , message , Toast.LENGTH_LONG).show()
    }

    protected fun showStatusBar() {
        (activity as MainActivity).showStatusBar()
    }
    protected fun hideStatusBar() {
        (activity as MainActivity).hideStatusBar()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
