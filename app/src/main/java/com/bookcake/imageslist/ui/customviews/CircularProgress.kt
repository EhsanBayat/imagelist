package com.bookcake.imageslist.ui.customviews

import android.animation.Animator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.Property
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.view.animation.Interpolator
import android.view.animation.LinearInterpolator
import com.bookcake.imageslist.R

class CircularProgress @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
    private var mColor: Int
    private val mSize: Int
    private val mIndeterminate: Boolean
    private var mMax: Int
    private var mProgress = 0
    private var mIndeterminateProgressDrawable: IndeterminateProgressDrawable? = null
    private var mDeterminateProgressDrawable: DeterminateProgressDrawable? = null
    fun setColor(color: Int) {
        mColor = color
        invalidate()
    }

    fun startAnimation() {
        if (visibility != VISIBLE) {
            return
        }
        mIndeterminateProgressDrawable!!.start()
    }

    fun stopAnimation() {
        mIndeterminateProgressDrawable!!.stop()
    }

    @get:Synchronized
    @set:Synchronized
    var max: Int
        get() = mMax
        set(max) {
            var max = max
            if (max < 0) {
                max = 0
            }
            if (max != mMax) {
                mMax = max
                postInvalidate()
                if (mProgress > max) {
                    mProgress = max
                }
            }
        }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var widthMeasureSpec = widthMeasureSpec
        var heightMeasureSpec = heightMeasureSpec
        val widthSpecMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightSpecMode = MeasureSpec.getMode(heightMeasureSpec)
        if (widthSpecMode == MeasureSpec.AT_MOST && heightSpecMode == MeasureSpec.AT_MOST) {
            var size = 0
            when (mSize) {
                SMALL_SIZE -> size =
                    resources.getDimensionPixelSize(R.dimen.circular_progress_small_size)
                NORMAL_SIZE -> size =
                    resources.getDimensionPixelSize(R.dimen.circular_progress_normal_size)
                LARGE_SIZE -> size =
                    resources.getDimensionPixelSize(R.dimen.circular_progress_large_size)
            }
            widthMeasureSpec = MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY)
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY)
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        if (mIndeterminate) {
            mIndeterminateProgressDrawable!!.draw(canvas)
        } else {
            mDeterminateProgressDrawable!!.draw(canvas)
        }
    }

    override fun onVisibilityChanged(changedView: View, visibility: Int) {
        super.onVisibilityChanged(changedView, visibility)
        if (mIndeterminate) {
            if (visibility == VISIBLE) {
                mIndeterminateProgressDrawable!!.start()
            } else {
                mIndeterminateProgressDrawable!!.stop()
            }
        }
    }

    override fun verifyDrawable(drawable: Drawable): Boolean {
        return if (mIndeterminate) {
            drawable === mIndeterminateProgressDrawable || super.verifyDrawable(drawable)
        } else {
            drawable === mDeterminateProgressDrawable || super.verifyDrawable(drawable)
        }
    }

    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        super.onSizeChanged(width, height, oldWidth, oldHeight)
        if (mIndeterminate) {
            mIndeterminateProgressDrawable!!.setBounds(0, 0, width, height)
        } else {
            mDeterminateProgressDrawable!!.setBounds(0, 0, width, height)
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (mIndeterminate) {
            startAnimation()
        }
    }

    override fun onDetachedFromWindow() {
        if (mIndeterminate) {
            stopAnimation()
        }
        super.onDetachedFromWindow()
    }

    private inner class DeterminateProgressDrawable internal constructor(
        color: Int,
        borderWidth: Int,
        angle: Int
    ) : Drawable() {
        private val mPaint: Paint
        private val mBorderWidth: Float
        private val mDrawableBounds = RectF()
        override fun draw(canvas: Canvas) {
            canvas.drawArc(mDrawableBounds, -90f, 20f, false, mPaint)
        }

        override fun setAlpha(alpha: Int) {
            mPaint.alpha = alpha
        }

        override fun setColorFilter(colorFilter: ColorFilter?) {
            mPaint.colorFilter = colorFilter
        }

        override fun getOpacity(): Int {
            return PixelFormat.TRANSPARENT
        }

        override fun onBoundsChange(bounds: Rect) {
            super.onBoundsChange(bounds)
            mDrawableBounds.left = bounds.left + mBorderWidth / 2f + .5f
            mDrawableBounds.right = bounds.right - mBorderWidth / 2f - .5f
            mDrawableBounds.top = bounds.top + mBorderWidth / 2f + .5f
            mDrawableBounds.bottom = bounds.bottom - mBorderWidth / 2f - .5f
        }

        init {
            mPaint = Paint()
            mPaint.isAntiAlias = true
            mPaint.style = Paint.Style.STROKE
            mPaint.strokeWidth = borderWidth.toFloat()
            mPaint.color = color
            mBorderWidth = borderWidth.toFloat()
        }
    }

    private inner class IndeterminateProgressDrawable internal constructor(
        color: Int,
        private val mBorderWidth: Float
    ) : Drawable(), Animatable {
        private val ANGLE_INTERPOLATOR: Interpolator = LinearInterpolator()
        private val SWEEP_INTERPOLATOR: Interpolator = DecelerateInterpolator()
        private val mDrawableBounds = RectF()
        private var mObjectAnimatorSweep: ObjectAnimator? = null
        private var mObjectAnimatorAngle: ObjectAnimator? = null
        private var mModeAppearing = false
        private val mPaint: Paint
        private var mCurrentGlobalAngleOffset = 0f
        private var mCurrentGlobalAngle = 0f
        private var mCurrentSweepAngle = 0f
        private var mRunning = false
        override fun draw(canvas: Canvas) {
            var startAngle = mCurrentGlobalAngle - mCurrentGlobalAngleOffset
            var sweepAngle = mCurrentSweepAngle
            if (!mModeAppearing) {
                startAngle = startAngle + sweepAngle
                sweepAngle = 360 - sweepAngle - MIN_SWEEP_ANGLE
            } else {
                sweepAngle += MIN_SWEEP_ANGLE.toFloat()
            }
            canvas.drawArc(mDrawableBounds, startAngle, sweepAngle, false, mPaint)
        }

        override fun setAlpha(alpha: Int) {
            mPaint.alpha = alpha
        }

        override fun setColorFilter(cf: ColorFilter?) {
            mPaint.colorFilter = cf
        }

        override fun getOpacity(): Int {
            return PixelFormat.TRANSPARENT
        }

        private fun toggleAppearingMode() {
            mModeAppearing = !mModeAppearing
            if (mModeAppearing) {
                mCurrentGlobalAngleOffset =
                    (mCurrentGlobalAngleOffset + MIN_SWEEP_ANGLE * 2) % 360
            }
        }

        override fun onBoundsChange(bounds: Rect) {
            super.onBoundsChange(bounds)
            mDrawableBounds.left = bounds.left + mBorderWidth / 2f + .5f
            mDrawableBounds.right = bounds.right - mBorderWidth / 2f - .5f
            mDrawableBounds.top = bounds.top + mBorderWidth / 2f + .5f
            mDrawableBounds.bottom = bounds.bottom - mBorderWidth / 2f - .5f
        }

        ///////////////////////////////////////// Animation /////////////////////////////////////////
        private val mAngleProperty: Property<IndeterminateProgressDrawable, Float> =
            object : Property<IndeterminateProgressDrawable, Float>(
                Float::class.java, "angle"
            ) {
                override fun get(`object`: IndeterminateProgressDrawable): Float {
                    return `object`.currentGlobalAngle
                }

                override fun set(`object`: IndeterminateProgressDrawable, value: Float) {
                    `object`.currentGlobalAngle = value
                }
            }
        private val mSweepProperty: Property<IndeterminateProgressDrawable, Float> =
            object : Property<IndeterminateProgressDrawable, Float>(
                Float::class.java, "arc"
            ) {
                override fun get(`object`: IndeterminateProgressDrawable): Float {
                    return `object`.currentSweepAngle
                }

                override fun set(`object`: IndeterminateProgressDrawable, value: Float) {
                    `object`.currentSweepAngle = value
                }
            }

        private fun setupAnimations() {
            mObjectAnimatorAngle = ObjectAnimator.ofFloat(this, mAngleProperty, 360f)
            mObjectAnimatorAngle!!.setInterpolator(ANGLE_INTERPOLATOR)
            mObjectAnimatorAngle!!.setDuration(ANGLE_ANIMATOR_DURATION.toLong())
            mObjectAnimatorAngle!!.setRepeatMode(ValueAnimator.RESTART)
            mObjectAnimatorAngle!!.setRepeatCount(ValueAnimator.INFINITE)
            mObjectAnimatorSweep =
                ObjectAnimator.ofFloat(this, mSweepProperty, 360f - MIN_SWEEP_ANGLE * 2)
            mObjectAnimatorSweep!!.setInterpolator(SWEEP_INTERPOLATOR)
            mObjectAnimatorSweep!!.setDuration(SWEEP_ANIMATOR_DURATION.toLong())
            mObjectAnimatorSweep!!.setRepeatMode(ValueAnimator.RESTART)
            mObjectAnimatorSweep!!.setRepeatCount(ValueAnimator.INFINITE)
            mObjectAnimatorSweep!!.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {}
                override fun onAnimationEnd(animation: Animator) {}
                override fun onAnimationCancel(animation: Animator) {}
                override fun onAnimationRepeat(animation: Animator) {
                    toggleAppearingMode()
                }
            })
        }

        override fun start() {
            if (isRunning) {
                return
            }
            mRunning = true
            mObjectAnimatorAngle!!.start()
            mObjectAnimatorSweep!!.start()
            invalidateSelf()
        }

        override fun stop() {
            if (!isRunning) {
                return
            }
            mRunning = false
            mObjectAnimatorAngle!!.cancel()
            mObjectAnimatorSweep!!.cancel()
            invalidateSelf()
        }

        override fun isRunning(): Boolean {
            return mRunning
        }

        var currentGlobalAngle: Float
            get() = mCurrentGlobalAngle
            set(currentGlobalAngle) {
                mCurrentGlobalAngle = currentGlobalAngle
                invalidateSelf()
            }
        var currentSweepAngle: Float
            get() = mCurrentSweepAngle
            set(currentSweepAngle) {
                mCurrentSweepAngle = currentSweepAngle
                invalidateSelf()
            }

        init {
            mPaint = Paint()
            mPaint.isAntiAlias = true
            mPaint.style = Paint.Style.STROKE
            mPaint.strokeWidth = mBorderWidth
            mPaint.color = color
            setupAnimations()
        }
    }

    companion object {
        private const val SMALL_SIZE = 0
        private const val NORMAL_SIZE = 1
        private const val LARGE_SIZE = 2

        private const val ANGLE_ANIMATOR_DURATION = 2000
        private const val SWEEP_ANIMATOR_DURATION = 600
        private const val MIN_SWEEP_ANGLE = 30
    }

    init {
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.CircularProgress)
        mColor = attributes.getColor(
            R.styleable.CircularProgress_circular_progress_color,
            resources.getColor(android.R.color.white)
        )
        mSize = attributes.getInt(
            R.styleable.CircularProgress_circular_progress_size,
            NORMAL_SIZE
        )
        mIndeterminate = attributes.getBoolean(
            R.styleable.CircularProgress_circular_progress_indeterminate,
            resources.getBoolean(R.bool.circular_progress_indeterminate)
        )
        val mBorderWidth = attributes.getDimensionPixelSize(
            R.styleable.CircularProgress_circular_progress_border_width,
            resources.getDimensionPixelSize(R.dimen.circular_progress_border_width)
        )
        mMax = attributes.getInteger(
            R.styleable.CircularProgress_circular_progress_max,
            resources.getInteger(R.integer.circular_progress_max)
        )
        attributes.recycle()
        if (mIndeterminate) {
            mIndeterminateProgressDrawable =
                IndeterminateProgressDrawable(mColor, mBorderWidth.toFloat())
            mIndeterminateProgressDrawable!!.setCallback(this)
        } else {
            mDeterminateProgressDrawable = DeterminateProgressDrawable(mColor, mBorderWidth, 0)
            mDeterminateProgressDrawable!!.setCallback(this)
        }
    }
}