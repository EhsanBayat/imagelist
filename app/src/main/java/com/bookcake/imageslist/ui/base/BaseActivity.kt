package com.bookcake.imageslist.ui.base

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.bookcake.imageslist.R
import com.bookcake.imageslist.ui.modules.main.MainActivity
import com.bookcake.imageslist.utils.SharedPreferencesUtil

open class BaseActivity<VB : ViewDataBinding>(val layout: Int) : AppCompatActivity() {

    private var _binding: ViewDataBinding? = null

    @Suppress("UNCHECKED_CAST")
    protected val binding: VB
        get() = _binding as VB

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme()
        showStatusBar()
        super.onCreate(savedInstanceState)
        _binding = DataBindingUtil.setContentView(this, layout);
    }

    private fun setTheme() {
        val isNightThemeEnabled = SharedPreferencesUtil.nightTheme
        if (isNightThemeEnabled) setTheme(R.style.AppThemeNight) else setTheme(R.style.AppTheme)
    }

    fun hideStatusBar() {
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            val layoutParams: WindowManager.LayoutParams = window.attributes
            layoutParams.layoutInDisplayCutoutMode =
                WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
        }
    }

    fun showStatusBar() {
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

    protected fun restartApp() {
        val packageName: String = this.packageName
        val defaultIntent: Intent? =
            this.packageManager.getLaunchIntentForPackage(packageName)
        if (defaultIntent != null) {
            defaultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            this.startActivity(defaultIntent)
            Runtime.getRuntime().exit(0)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
