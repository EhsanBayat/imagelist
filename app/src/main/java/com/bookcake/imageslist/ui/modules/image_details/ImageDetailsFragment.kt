package com.bookcake.imageslist.ui.modules.image_details

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.navigation.fragment.navArgs
import com.bookcake.imageslist.R
import com.bookcake.imageslist.common.GlideApp
import com.bookcake.imageslist.extensions.makeGone
import com.bookcake.imageslist.extensions.makeVisible
import com.bookcake.imageslist.databinding.FragmentImageDetailsBinding
import com.bookcake.imageslist.ui.base.BaseFragment
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

class ImageDetailsFragment : BaseFragment<FragmentImageDetailsBinding>(R.layout.fragment_image_details) {

    private val args: ImageDetailsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar()
        initViews()
    }

    private fun initToolbar() {
        showHideToolbarItems(hasBack = true , toolbarTitle = R.string.image_details)
    }

    private fun initViews() {
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            imageBean = args.imageBean
            GlideApp.with(requireContext())
                .load(args.imageBean.largeImageURL)
                .into(imgOriginal)
            aviLoading.makeVisible()
            GlideApp.with(requireContext())
                .load(args.imageBean.largeImageURL)
                .placeholder(R.drawable.gallery_placeholder)
                .listener(object : RequestListener<Drawable?> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any,
                        target: Target<Drawable?>,
                        isFirstResource: Boolean
                    ): Boolean {
                        // log exception
                        Log.e("TAG", "Error loading image", e)
                        aviLoading.makeGone()
                        handleException("Error loading image!!!")

                        return false // important to return false so the error placeholder can be placed
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any,
                        target: Target<Drawable?>,
                        dataSource: DataSource,
                        isFirstResource: Boolean
                    ): Boolean {
                        aviLoading.makeGone()
                        return false
                    }
                })
                .into(imgOriginal)
            executePendingBindings()
        }

    }
}
