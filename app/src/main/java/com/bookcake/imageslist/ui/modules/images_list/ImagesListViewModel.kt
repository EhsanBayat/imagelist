package com.bookcake.imageslist.ui.modules.images_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bookcake.imageslist.data.pojo.SearchBean
import com.bookcake.imageslist.data.repository.ImagesListRepository
import com.bookcake.imageslist.ui.base.BaseViewModel
import com.bookcake.imageslist.data.network.ResultWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.get

class ImagesListViewModel : BaseViewModel(),KoinComponent {

    val loadingVisibility : MutableLiveData<Boolean> = MutableLiveData()
    private val _searchBean = MutableLiveData<ResultWrapper<SearchBean>>()
    val searchResult: LiveData<ResultWrapper<SearchBean>>
        get() = _searchBean


    private val repository: ImagesListRepository by lazy {
        ImagesListRepository(get())
    }

    fun fetchImages(searchQuery : String) {
        viewModelScope.launch(Dispatchers.IO) {
            loadingVisibility.postValue(true)
            val response = repository.fetchImages(searchQuery)
            loadingVisibility.postValue(false)
            _searchBean.postValue(response)
        }
    }
}

