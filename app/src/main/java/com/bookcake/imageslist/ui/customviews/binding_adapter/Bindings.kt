package com.bookcake.imageslist.ui.customviews.binding_adapter

import android.view.View
import androidx.databinding.BindingAdapter

object Bindings {
    @JvmStatic
    @BindingAdapter("bind:show")
    fun bindVisibility(view : View , visible: Boolean? = false) {
        view.visibility = if (visible == true) View.VISIBLE else View.GONE
    }
}