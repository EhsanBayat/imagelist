package com.bookcake.imageslist.ui.base

import androidx.lifecycle.ViewModel
/**
 * Base viewModel to provide needed features in all Child class viewModels.
 */
open class BaseViewModel : ViewModel() {}
