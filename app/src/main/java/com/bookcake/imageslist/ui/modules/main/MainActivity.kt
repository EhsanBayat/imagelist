package com.bookcake.imageslist.ui.modules.main

import androidx.navigation.fragment.NavHostFragment
import com.bookcake.imageslist.R
import com.bookcake.imageslist.extensions.makeGone
import com.bookcake.imageslist.extensions.makeVisible
import com.bookcake.imageslist.databinding.ActivityMainBinding
import com.bookcake.imageslist.ui.base.BaseActivity
import com.bookcake.imageslist.utils.SharedPreferencesUtil

class MainActivity : BaseActivity<ActivityMainBinding>(R.layout.activity_main) {

    private val navController by lazy {
        (supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment).navController
    }

    private fun hideAllToolbarItems() {
        binding.toolbar.apply {
            imgBack.makeGone()
            txtTitle.makeGone()
            imgTheme.makeGone()
        }
    }

    fun hideToolbar() {
        binding.toolbar.clParent.makeGone()
    }

    fun showHideToolbarItems(hasBack : Boolean? = null ,
                             hasTheme : Boolean? = null,
                             toolbarTitle : Int? = null) {
        binding.toolbar.clParent.makeVisible()
        hideAllToolbarItems()
        hasBack?.let {
            binding.toolbar.imgBack.apply {
                makeVisible()
                setOnClickListener{
                    navController.navigateUp()
                }
            }
        }
        hasTheme?.let {
            binding.toolbar.imgTheme.apply {
                makeVisible()
                setOnClickListener{
                    val isNightThemeEnabled = SharedPreferencesUtil.nightTheme
                    SharedPreferencesUtil.nightTheme = !isNightThemeEnabled
                    recreate()
                }
            }
        }
        toolbarTitle?.let {
            binding.toolbar.txtTitle.apply {
                makeVisible()
                text = resources.getString(it)
            }
        }
    }
}