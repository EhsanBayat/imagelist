package com.bookcake.imageslist.ui.modules.images_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bookcake.imageslist.common.GlideApp
import com.bookcake.imageslist.extensions.getContext
import com.bookcake.imageslist.data.pojo.ImageBean
import com.bookcake.imageslist.databinding.ItemImageBinding

class ImagesListAdapter(private val getImageInfo: (ImageBean) -> Unit) :
    ListAdapter<ImageBean, ImagesListAdapter.ViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemImageBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            getImageInfo
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val imageBean = getItem(position)
        holder.apply {
            bind(imageBean)
            itemView.tag = imageBean
        }
    }

    class ViewHolder(
        private val binding: ItemImageBinding,
        private val getImageInfo: (ImageBean) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ImageBean) {
            itemView.setOnClickListener { getImageInfo(item) }
            binding.apply {
                imageBean = item
                GlideApp.with(getContext())
                    .load(item.previewURL)
                    .into(imgThumbnail)
                executePendingBindings()
            }
        }
    }

    private class DiffCallback : DiffUtil.ItemCallback<ImageBean>() {

        override fun areItemsTheSame(oldItem: ImageBean, newItem: ImageBean): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: ImageBean, newItem: ImageBean): Boolean {
            return oldItem == newItem
        }
    }
}
