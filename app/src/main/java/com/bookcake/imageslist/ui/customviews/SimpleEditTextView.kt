package com.bookcake.imageslist.ui.customviews

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.widget.EditText
import androidx.appcompat.widget.AppCompatEditText


class SimpleEditTextView(context: Context, attrs: AttributeSet?) :
    AppCompatEditText(context, attrs) {
    override fun setError(error: CharSequence?, icon: Drawable?) {
        setCompoundDrawables(null, null, icon, null)
    }
}