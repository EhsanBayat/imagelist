package com.bookcake.imageslist.extensions

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build

@SuppressLint("MissingPermission")
fun Any.isConnectToInternet(): Boolean {

    val connectivityManager =
        getContext().getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        val networks = connectivityManager?.allNetworks
        networks?.let {
            var networkInfo: NetworkInfo
            for (mNetwork in networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork)!!
                if (networkInfo.state == NetworkInfo.State.CONNECTED)
                    return true
            }
        }
    } else {
        val info = connectivityManager?.allNetworkInfo
        if (info != null)
            for (anInfo in info)
                if (anInfo.state == NetworkInfo.State.CONNECTED)
                    return true
    }
    return false
}
