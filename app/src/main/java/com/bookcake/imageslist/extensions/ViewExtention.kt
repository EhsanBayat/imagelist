package com.bookcake.imageslist.extensions

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
/**
 * Hides keyboard.
 *
 * @param context The context of view that hide keyboard is to be done.
 */
fun View.hideKeyboard(context: Context) {
    val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputManager.hideSoftInputFromWindow(windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
}

fun View.showKeyboard(context: Context) {
    requestFocus()
    val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputManager.showSoftInput(this, InputMethodManager.SHOW_FORCED)
}

/**
 * Makes a view visible.
 *
 */
fun View.makeVisible() {
    visibility = View.VISIBLE
}

/**
 * Makes a view invisible.
 *
 */
fun View.makeInVisible() {
    visibility = View.INVISIBLE
}

/**
 * Makes a view clickable.
 *
 */
fun View.makeClickable() {
    isClickable = true
}

/**
 * Makes a view don't clickable.
 *
 */
fun View.makeNotClickable() {
    isClickable = false
}

/**
 * Makes a view gone.
 *
 */
fun View.makeGone() {
    visibility = View.GONE
}

/**
 * set background color of view
 *
 * @param color
 */
fun View.setBackGround(color: Int) {
    setBackgroundColor(ContextCompat.getColor(this.context, color))
}

