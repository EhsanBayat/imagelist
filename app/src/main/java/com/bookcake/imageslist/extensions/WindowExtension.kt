package com.bookcake.imageslist.extensions

import android.view.Window

fun Window.getSoftInputMode(): Int {
    return attributes.softInputMode
}