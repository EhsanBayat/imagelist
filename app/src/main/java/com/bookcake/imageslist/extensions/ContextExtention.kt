package com.bookcake.imageslist.extensions

import android.content.Context
import android.os.Build
import android.view.autofill.AutofillManager
import androidx.annotation.RequiresApi
import com.bookcake.imageslist.common.BaseApplication

fun Any.getContext(): Context {
    return BaseApplication.instance
}

@RequiresApi(Build.VERSION_CODES.O)
fun Any.getAutofillManager() : AutofillManager {
    return getContext().getSystemService(AutofillManager::class.java)
}



