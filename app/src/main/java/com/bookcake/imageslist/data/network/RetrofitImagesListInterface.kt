package com.bookcake.imageslist.data.network

import com.bookcake.imageslist.BuildConfig
import com.bookcake.imageslist.data.pojo.SearchBean
import retrofit2.http.*

private const val IMAGE_TYPE = "photo"
interface RetrofitImagesListInterface {
    /**
     * @param q means query param for search
     */
    @GET(BuildConfig.API_KEY)
    suspend fun searchImage(
        @Query("q") searchQuery: String,
        @Query("image_type") imageType: String = IMAGE_TYPE,
    ): SearchBean
}