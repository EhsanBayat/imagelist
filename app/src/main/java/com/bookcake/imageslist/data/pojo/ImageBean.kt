package com.bookcake.imageslist.data.pojo

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImageBean(
    val id: String,
    val pageURL: String? = null,
    val type : String? = null,
    val tags : String? = null,
    val previewURL : String? = null,
    val previewWidth: String? = null,
    @SerializedName("webformatURL")
    val webFormatURL: String? = null,
    @SerializedName("webformatWidth")
    val webFormatWidth: String? = null,
    @SerializedName("webformatHeight")
    val webFormatHeight: String? = null,
    val largeImageURL: String? = null,
    val imageWidth: String? = null,
    val imageHeight: String? = null,
    val imageSize: String? = null,
    val views: String? = null,
    val downloads: String? = null,
    val collections: String? = null,
    val likes: String? = null,
    val comments: String? = null,
    val user_id: String? = null,
    val user: String? = null,
    val userImageURL: String? = null
): Parcelable