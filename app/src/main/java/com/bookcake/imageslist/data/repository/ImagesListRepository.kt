package com.bookcake.imageslist.data.repository

import com.bookcake.imageslist.data.network.BaseDataSource
import com.bookcake.imageslist.data.network.NetworkManagerInterface

class ImagesListRepository(val network: NetworkManagerInterface) : BaseDataSource(){

    suspend fun fetchImages(searchQuery : String) = getResult {
        network.searchImage(searchQuery = searchQuery)
    }

}