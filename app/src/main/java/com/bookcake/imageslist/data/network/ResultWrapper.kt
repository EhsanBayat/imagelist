package com.bookcake.imageslist.data.network

sealed class ResultWrapper<out T> {
    data class Success<out T>(val data: T? = null) : ResultWrapper<T>()
    data class Failure(val message: String): ResultWrapper<Nothing>()
}

