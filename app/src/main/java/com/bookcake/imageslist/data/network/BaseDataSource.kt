package com.bookcake.imageslist.data.network

import org.koin.core.KoinComponent
import retrofit2.HttpException

abstract class BaseDataSource() : KoinComponent {
    protected suspend fun <T> getResult(apiCall: suspend () -> T): ResultWrapper<T> {
        return try {
            ResultWrapper.Success(apiCall.invoke())
            } catch (throwable: Throwable) {
                when (throwable) {
                    is HttpException -> {
                        ResultWrapper.Failure("error code : " + throwable.code() + "\n" +
                                                throwable.message())
                    }
                    else -> {
                        ResultWrapper.Failure("unknown error occurred!!!")
                    }
            }
        }
    }
}

