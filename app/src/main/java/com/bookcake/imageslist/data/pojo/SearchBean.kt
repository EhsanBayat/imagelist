package com.bookcake.imageslist.data.pojo

import com.google.gson.annotations.SerializedName

data class SearchBean(
    val total: String,
    val totalHits: String? = null,
    @SerializedName("hits")
    val imagesList: List<ImageBean>? = null
)