package com.bookcake.imageslist.common

const val DIALOG_WIDTH = 300
const val SPLASH_TIME_OUT = 1000L
const val NIGHT_THEME = "nightTheme"

