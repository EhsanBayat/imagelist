package com.bookcake.imageslist.common

import android.app.Application
import com.bookcake.imageslist.BuildConfig
import com.bookcake.imageslist.di.sharedPreferencesModule
import com.bookcake.imageslist.di.viewModelModule
import com.bookcake.imageslist.di.dataRepositoryModule
import com.bookcake.imageslist.di.gsonConverterFactoryModule
import com.bookcake.imageslist.di.gsonModule
import com.bookcake.imageslist.di.retrofitModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class BaseApplication : Application() {

    companion object {
        @JvmStatic
        lateinit var instance: BaseApplication
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        initKoin()
        initLogger()

    }

    private fun initKoin() {
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@BaseApplication)
            modules(
                gsonConverterFactoryModule,
                gsonModule,
                dataRepositoryModule,
                retrofitModule,
                sharedPreferencesModule,
                viewModelModule
            )
        }
    }

    private fun initLogger() {
        if (BuildConfig.DEBUG) Timber.plant(object : Timber.DebugTree() {
            override fun createStackElementTag(element: StackTraceElement): String? {
                return "${element.className} -> method:${element.methodName} -> ${element.lineNumber}"
            }
        })
    }
}
